#include <chrono>

#include "common/math.hpp"


struct GameClock {
    std::chrono::time_point<std::chrono::system_clock> start_systime;
    f64 start_gametime;

    /**
     * @brief Get the time in elapsed game seconds
     * 
     * @return f64 
     */
    f64 get_time() {
        auto t = std::chrono::high_resolution_clock::now();
        return static_cast<f64>(std::chrono::duration_cast<std::chrono::seconds>(t - start_systime).count());
    };

    /**
     * @brief Construct a new Game Clock object, starting from time = 0
     * 
     */
    GameClock() {
        start_systime = std::chrono::high_resolution_clock::now();
        start_gametime = 0.0;
    }
};
