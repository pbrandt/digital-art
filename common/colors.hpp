
#include "common/math.hpp"

struct RGBA {
    u8 r;
    u8 g;
    u8 b;
    u8 a;
};

struct HSV {
    f64 h; // [0, 360]
    f64 s; // [0, 1]
    f64 v; // [0, 1]
};

static inline RGBA hsv2rgb(HSV color) {
    RGBA c;
    f64 r = 0;
    f64 g = 0;
    f64 b = 0;

    
    f64 hue = clamp360<f64>(color.h); // [0, 360]
    f64 sat = clamp<f64>(color.s, 0, 1); // [0, 1]
    f64 val = clamp<f64>(color.v, 0, 1); // [0, 1]

    // from wikipedia https://en.wikipedia.org/wiki/HSL_and_HSV#From_HSV
    f64 chroma = sat * val;
    f64 hue_prime = hue / 60.0;
    f64 x = chroma * ( 1 - abs(fmod(hue_prime, 2.0) - 1.0) );
    if (hue_prime >= 0 && hue_prime < 1.0) {
        r = chroma;
        g = x;
    } else if (hue_prime >= 1.0 && hue_prime < 2.0) {
        r = x;
        g = chroma;
    } else if (hue_prime >= 2.0 && hue_prime < 3.0) {
        g = chroma;
        b = x;
    } else if (hue_prime >= 3.0 && hue_prime < 4.0) {
        g = x;
        b = chroma;
    } else if (hue_prime >= 4.0 && hue_prime < 5.0) {
        r = x;
        b = chroma;
    } else if (hue_prime >= 5.0 && hue_prime <= 6.0) {
        r = chroma;
        b = x;
    }

    f64 match = val - chroma;
    r += match;
    b += match;

    // convert rgb from [0,1] to [0, 255]
    c.r = r * 255;
    c.g = g * 255;
    c.b = b * 255;

    // set alpha to opaque
    c.a = 255;

    return c;
};

static inline HSV rgb2hsv(RGBA color) {
    HSV c;
    f64 r = static_cast<f64>(color.r) / 255.0;
    f64 g = static_cast<f64>(color.g) / 255.0;
    f64 b = static_cast<f64>(color.b) / 255.0;

    f64 xmax = std::max(std::max(r, b), g);
    f64 xmin = std::min(std::min(r, b), g);
    f64 val = xmax;
    f64 chroma = xmax - xmin;

    if (chroma < 1.0e-8) {
        c.h = 0.0;
    } else if (val == r) {
        c.h = 60.0 * (g - b) / chroma;
    } else if (val == g) {
        c.h = 60.0 * ( 2.0 + (b - r) / chroma );
    } else if (val == b) {
        c.h = 60.0 * ( 4.0 + (r - g) / chroma );
    }

    c.v = val;
    c.s = val == 0.0 ? 0.0 : chroma / val;

    return c;
};

