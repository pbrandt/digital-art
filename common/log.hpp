#include <cstdio>
#include <cstdarg>

static void debug(const char *fmt, ...) {
    #ifdef NDEBUG
    // Boop
    #else
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    #endif
}
