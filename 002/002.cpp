#include <stdlib.h>
#include <cstdio>

#include <SDL2/SDL.h>

#include "common/math.hpp"
#include "common/clock.hpp"

// 16:9 portrait mode
#define WINDOW_WIDTH 450
#define WINDOW_HEIGHT 800

SDL_Event event;
SDL_Renderer *renderer;
SDL_Surface *surface;
SDL_Window *window;
GameClock gameclock;

// Animate something, anything.
void render(double t) {
    if (SDL_MUSTLOCK(surface)) {
        SDL_LockSurface(surface);
    }

    u8 *pixels = static_cast<u8*>(surface->pixels);

    for (u32 i = 0; i < WINDOW_HEIGHT * WINDOW_WIDTH * 4; i+= 4) {
        pixels[i] = rand() % 255; // r
        pixels[i+1] = rand() % 255; // g
        pixels[i+2] = rand() % 255; // b
        // pixels[i] = 255; // alpha
    }

    if (SDL_MUSTLOCK(surface)) {
        SDL_UnlockSurface(surface);
    }

    SDL_Texture *screenTexture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, screenTexture, NULL, NULL);
    SDL_RenderPresent(renderer);
    SDL_DestroyTexture(screenTexture);
}

int main(void) {
    double t = 0;

    // Set up the window
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_HEIGHT, 0, &window, &renderer);
    surface = SDL_CreateRGBSurface(0, WINDOW_WIDTH, WINDOW_HEIGHT, 32, 0, 0, 0, 0);
    
    // Render loop
    bool done = false;
    while (!done) {

        // event loop, parse all events this past render cycle
        while (SDL_PollEvent(&event)) {
            // OS window "close button" pressed
            if (event.type == SDL_QUIT) {
                done = true;
            }
            
            // key pressed
            if (event.type == SDL_KEYDOWN) {
                // escape = quit
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    done = true;
                }
            }
        }
        t = gameclock.get_time();
        render(t);
        // std::cout << "T " << t << " next_frame " <<  next_frame << " sleeping " << ms << std::endl;
        SDL_Delay(30);
    }

    // leave
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
