#!/usr/bin/env bash

set -e

GIT_ROOT=$(git rev-parse --show-toplevel)

clang++ -g -Wall -o 002.exe 002.cpp -lSDL2 -I${GIT_ROOT}

./002.exe

