#!/usr/bin/env bash

set -e

GIT_ROOT=$(git rev-parse --show-toplevel)

clang++ -g -Wall -o 003.exe 003.cpp -lSDL2 -I${GIT_ROOT}

./003.exe

