#include <stdlib.h>
#include <cstdio>
#include <cstring>

#include <SDL2/SDL.h>

#include "common/math.hpp"
#include "common/clock.hpp"
#include "common/log.hpp"

// 16:9 portrait mode
#define WINDOW_WIDTH 450
#define WINDOW_HEIGHT 800

SDL_Event event;
SDL_Renderer *renderer;
SDL_Surface *surface;
SDL_Window *window;
GameClock gameclock;

f64 heightmap_a[WINDOW_WIDTH][WINDOW_HEIGHT];
f64 heightmap_b[WINDOW_WIDTH][WINDOW_HEIGHT];

inline void draw_pixel(u32 x, u32 y, u8 r, u8 g, u8 b, u8 a) {
    u8 *pixels = static_cast<u8*>(surface->pixels);
    u32 coord = y * WINDOW_WIDTH * 4 + x * 4;
    // debug("x: %i, y: %i, coord: %i\n", x, y, coord);
    pixels[coord] = r;
    pixels[coord + 1] = g;
    pixels[coord + 2] = b;
    pixels[coord + 3] = a;
}


// Animate a charged curve x^3 + x^3 - axy = 0
void render(double t) {
    if (SDL_MUSTLOCK(surface)) {
        SDL_LockSurface(surface);
    }

    // Perform averaging process
    for (u32 i = 1; i < WINDOW_WIDTH - 1; i++) {
        for (u32 j = 1; j < WINDOW_HEIGHT - 1; j++) {
            heightmap_b[i][j] = (heightmap_a[i+1][j] + heightmap_a[i-1][j] + heightmap_a[i][j+1] + heightmap_a[i][j-1]) / 4.0;
        }
    }

    // Reset voltage sources
    for (u32 i = 1; i < WINDOW_WIDTH - 1; i++) {
        for (u32 j = 1; j < WINDOW_HEIGHT - 1; j++) {
            if (16 * i == 9 * j) {
                heightmap_b[i][j] = 100;
            }
        }
    }

    // Copy buffer to finish
    memcpy(&heightmap_a, &heightmap_b, WINDOW_WIDTH*WINDOW_HEIGHT * sizeof(f64));

    // Draw
    for (u32 i = 0; i < WINDOW_WIDTH; i++) {
        for (u32 j = 0; j < WINDOW_HEIGHT; j++) {
            // even odd striping for voltages
            // debug("i: %i, j: %i, h: %f, mod: %i\n", i, j, heightmap_a[i][j], static_cast<i32>(heightmap_a[i][j]) % 2);
            if ((static_cast<i32>(heightmap_a[i][j]) / 8) % 2 == 0) {
                draw_pixel(i, j, 0, 0, 0, 0);
            } else {
                draw_pixel(i, j, rand() % 127 + 127, rand() % 127 + 127, rand() % 127 + 127, 255);
            }
        }
    }

    if (SDL_MUSTLOCK(surface)) {
        SDL_UnlockSurface(surface);
    }

    SDL_Texture *screenTexture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, screenTexture, NULL, NULL);
    SDL_RenderPresent(renderer);
    SDL_DestroyTexture(screenTexture);
}

int main(void) {
    double t = 0;

    // Set up the window
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_HEIGHT, 0, &window, &renderer);
    surface = SDL_CreateRGBSurface(0, WINDOW_WIDTH, WINDOW_HEIGHT, 32, 0, 0, 0, 0);
    
    // Render loop
    bool done = false;
    while (!done) {

        // event loop, parse all events this past render cycle
        while (SDL_PollEvent(&event)) {
            // OS window "close button" pressed
            if (event.type == SDL_QUIT) {
                done = true;
            }
            
            // key pressed
            if (event.type == SDL_KEYDOWN) {
                // escape = quit
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    done = true;
                }
            }
        }
        t = gameclock.get_time();
        render(t);
        // std::cout << "T " << t << " next_frame " <<  next_frame << " sleeping " << ms << std::endl;
        SDL_Delay(1);
    }

    // leave
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
