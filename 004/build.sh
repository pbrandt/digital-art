#!/usr/bin/env bash

set -e

GIT_ROOT=$(git rev-parse --show-toplevel)

clang++ -g -Wall -o 004.exe 004.cpp -lSDL2 -I${GIT_ROOT}

./004.exe

