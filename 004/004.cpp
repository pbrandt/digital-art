#include <stdlib.h>
#include <cstdio>
#include <cstring>

#include <SDL2/SDL.h>

#include "common/math.hpp"
#include "common/clock.hpp"
#include "common/log.hpp"
#include "common/colors.hpp"

// 16:9 portrait mode
#define WINDOW_WIDTH 450
#define WINDOW_HEIGHT 800

SDL_Event event;
SDL_Renderer *renderer;
SDL_Surface *surface;
SDL_Window *window;
GameClock gameclock;

complex128 zmap[WINDOW_WIDTH][WINDOW_HEIGHT] = {};
u8 zmapcolor[WINDOW_WIDTH][WINDOW_HEIGHT] = {};
u8 zmapmask[WINDOW_WIDTH][WINDOW_HEIGHT] = {};
u8 Z_DIVERGENT = 1;
u8 Z_CONVERGENT = 0;

// parameter to explore
complex128 mu = {0.462, -0.21};
f64 REAL_AXES = 2.0;
f64 IMAG_AXES = REAL_AXES * WINDOW_HEIGHT / WINDOW_WIDTH;
f64 threshold = 100;


inline void draw_pixel(u32 x, u32 y, u8 r, u8 g, u8 b, u8 a) {
    u8 *pixels = static_cast<u8*>(surface->pixels);
    u32 coord = y * WINDOW_WIDTH * 4 + x * 4;
    // debug("x: %i, y: %i, coord: %i\n", x, y, coord);
    pixels[coord] = r;
    pixels[coord + 1] = g;
    pixels[coord + 2] = b;
    pixels[coord + 3] = a;
}

inline RGBA get_pixel(u32 x, u32 y) {
    u8 *pixels = static_cast<u8*>(surface->pixels);
    u32 coord = y * WINDOW_WIDTH * 4 + x * 4;
    RGBA c = {
        pixels[coord],
        pixels[coord + 1],
        pixels[coord + 2],
        pixels[coord + 3]
    };
    return c;
}


// julia set
void render(double t) {
    if (SDL_MUSTLOCK(surface)) {
        SDL_LockSurface(surface);
    }

    static u32 frame = 0;
    frame++;

    // Apply the julia function or whatever it's called
    for (u32 i = 0; i < WINDOW_WIDTH; i++) {
        for (u32 j = 0; j < WINDOW_HEIGHT; j++) {
            if (zmapmask[i][j] == Z_DIVERGENT) {
                // rotate hue just a little bit
                RGBA c1 = get_pixel(i, j);
                HSV c1hsv = rgb2hsv(c1);
                c1hsv.h = clamp360<f64>(c1hsv.h - 1);
                RGBA c2 = hsv2rgb(c1hsv);
                draw_pixel(i, j, c2.r, c2.g, c2.b, 255);
                continue; // already diverged, don't keep calculating this one
            }

            complex128 z = zmap[i][j];
            // debug("i %i j %i real %f imag %f\n", i, j, z.real(), z.imag());
            // z = z * z + mu;
            z = pow(z, 1.232542) + mu;
            zmap[i][j] = z;
            if (abs(z) > threshold) {
                zmapmask[i][j] = Z_DIVERGENT;
                HSV color;
                color.h = clamp360<f64>(frame * 12 + 120);
                color.s = 1.0;
                color.v = clamp<f64>(frame * 0.1, 0, 1);
                RGBA rgb = hsv2rgb(color);

                draw_pixel(i, j, rgb.r, rgb.g, rgb.b, 255);
            } else {
                draw_pixel(i, j, 0, 0, 0, 255);
            }
        }
    }

    if (SDL_MUSTLOCK(surface)) {
        SDL_UnlockSurface(surface);
    }

    SDL_Texture *screenTexture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, screenTexture, NULL, NULL);
    SDL_RenderPresent(renderer);
    SDL_DestroyTexture(screenTexture);
}

int main(void) {
    f64 t = 0;
    u32 frame = 0;

    // Set up the window
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_HEIGHT, 0, &window, &renderer);
    surface = SDL_CreateRGBSurface(0, WINDOW_WIDTH, WINDOW_HEIGHT, 32, 0, 0, 0, 0);

    // initialize complex plane map
    for (u32 i = 0; i < WINDOW_WIDTH; i++) {
        for (i32 j = 0; j < WINDOW_HEIGHT; j++) {
            complex128 z = {
                (static_cast<f64>(j - 100) / WINDOW_HEIGHT - 0.5) * 2.0 * IMAG_AXES,
                (static_cast<f64>(i) / WINDOW_WIDTH - 0.5) * 2.0 * REAL_AXES
            };
            zmap[i][j] = z;
            // debug("i %i j %i real %f imag %f\n", i, j, z.real(), z.imag());
        }
    }

    debug("\n\n\n");
    
    // Render loop
    bool done = false;
    bool ACTIVATE_THE_MECHANISM = false;
    while (!done) {

        // event loop, parse all events this past render cycle
        while (SDL_PollEvent(&event)) {
            // OS window "close button" pressed
            if (event.type == SDL_QUIT) {
                done = true;
            }
            
            // key pressed
            if (event.type == SDL_KEYDOWN) {
                // escape = quit
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    done = true;
                } else if (event.key.keysym.sym == SDLK_RETURN) {
                    ACTIVATE_THE_MECHANISM = true;
                }
            }
        }
        t = gameclock.get_time();
        // debug("FRAME%s", "\n");
        if (ACTIVATE_THE_MECHANISM || frame == 0) {
            render(t);
        }
        // std::cout << "T " << t << " next_frame " <<  next_frame << " sleeping " << ms << std::endl;
        SDL_Delay(1000 / 30);
        frame++;
    }

    // leave
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
