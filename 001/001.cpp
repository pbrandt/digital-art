#include <stdlib.h>
#include <chrono>
#include <cstdio>

#include <SDL2/SDL.h>

// 16:9 portrait mode
#define WINDOW_WIDTH 450
#define WINDOW_HEIGHT 800
#define FRAME_RATE 30

static std::chrono::time_point<std::chrono::system_clock> start_time;

inline double play_time() {
    auto now = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = now - start_time;
    return elapsed_seconds.count();
}

// Animate something, anything.
void render(SDL_Renderer* renderer, double t) {
    int i;
    static int source = 0;
    static int sink = 1;
    static uint8_t rgb[3] = {255, 0, 0};

    if (rgb[source] == 0) {
        source = (source + 1) % 3;
        sink = (source + 1) % 3;
    }
    rgb[source] = rgb[source] - 5;
    rgb[sink] = rgb[sink] + 5;

    // printf("source %i sink %i rgb %i,%i,%i\n", source, sink, rgb[0], rgb[1], rgb[2]);
    
    SDL_SetRenderDrawColor(renderer, rgb[0], rgb[1], rgb[2], 255);
    for (i = 0; i < WINDOW_WIDTH; i++) {
        SDL_RenderDrawPoint(renderer, i, i);
    }
    SDL_RenderPresent(renderer);
}

int main(void) {
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    uint32_t frame = 0;
    double t = 0;
    double next_frame = 0;
    uint32_t ms;

    // Set up the window
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_HEIGHT, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    start_time = std::chrono::system_clock::now();


    // wait for escape
    while (1) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT) {
            break;
        }
        render(renderer, t);
        frame++;
        next_frame = next_frame + 1.0 / FRAME_RATE;
        t = play_time();
        ms = static_cast<uint32_t>( (next_frame - t) * 1000 );
        // std::cout << "T " << t << " next_frame " <<  next_frame << " sleeping " << ms << std::endl;
        SDL_Delay(ms);
    }

    // leave
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
